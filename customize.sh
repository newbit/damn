# Set to true if you do *NOT* want Magisk to mount
# any files for you. Most modules would NOT want
# to set this flag to true
SKIPMOUNT=false

# Set to true if you need to load system.prop
PROPFILE=false

# Set to true if you need post-fs-data script
POSTFSDATA=true

# Set to true if you need late_start service script
LATESTARTSERVICE=false

# Construct your own list here
REPLACE="
"

print_banner() {
	VERSION=$(grep_prop version "${TMPDIR}/module.prop")
	ID=$(grep_prop id "${TMPDIR}/module.prop")
	NAME=$(grep_prop name "${TMPDIR}/module.prop")
	ui_print ""
	ui_print "                ${ID}               "
	ui_print "  ${NAME} ${VERSION}  "
	ui_print "          by NewBit @ XDA           "
	ui_print ""
}

enforce_install_from_app() {
  if $BOOTMODE; then
    ui_print "- Installing from Magisk app"
  else
    ui_print "*********************************************************"
    ui_print "! Install from recovery is NOT supported"
    ui_print "! Recovery sucks"
    ui_print "! Please install from Magisk / KernelSU app"
    abort "*********************************************************"
  fi
}

extract_files() {
	ui_print "- extracting Module files "
	unzip -o "$ZIPFILE" 'CHANGELOG.md' -d $MODPATH >&2
	unzip -o "$ZIPFILE" 'module.prop' -d $MODPATH >&2
	unzip -o "$ZIPFILE" 'uninstall.sh' -d $MODPATH >&2
	unzip -o "$ZIPFILE" 'customize.sh' -d $MODPATH >&2
	unzip -o "$ZIPFILE" 'post-fs-data.sh' -d $MODPATH >&2
	unzip -o "$ZIPFILE" 'README.md' -d $MODPATH >&2
}

print_banner
enforce_install_from_app
extract_files

set_perm_recursive "$MODPATH" 0 0 0755 0644



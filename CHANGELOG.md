**v1.0.3**
- upgraded to newer Magisk Module Structure
- added automated release script
- migrated to gitlab

**v1.0.2**
- added adb shell "touch /data/local/tmp/damn"

**v1.0.1**
- added zip file to release section

**v1.0.0**
- initial release
